package com.mushynskyi.mytree;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;

public class MyTree<K extends Comparable<K>, V> implements Map<K, V> {

  private static final boolean RED = true;
  private static final boolean BLACK = false;
  private static Logger logger = LogManager.getLogger(MyTree.class);

  private int size;

  private Node<K, V> root;
  private Comparator<? super K> comparator;

  public MyTree() {
    size = 0;
    root = null;
    comparator = null;
  }

  static class Node<K, V> {
    K key;
    V value;
    boolean color;
    Node<K, V> left;
    Node<K, V> right;

    public Node(K key, V value, boolean col) {
      this.key = key;
      this.value = value;
      this.color = col;
    }
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean isEmpty() {
    if (size == 0)
      return true;
    return false;
  }

  @Override
  public boolean containsKey(Object key) {
    if (key == null) {
      logger.error("Your key is null");
    }
    Node<K, V> current = root;
    while (current != null) {
      int compareResult = compare((K) key, current.key);
      if (compareResult < 0) {
        current = current.left;
      } else if (compareResult > 0) {
        current = current.right;
      } else {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean containsValue(Object value) {
    return false;
  }

  @Override
  public V get(Object key) {
    if (key == null) {
      logger.error("Your key is null");
    }
    Node<K, V> current = root;
    while (current != null) {
      int compareResult = compare((K) key, current.key);
      if (compareResult < 0) {
        current = current.left;
      } else if (compareResult > 0) {
        current = current.right;
      } else {
        return current.value;
      }
    }
    return null;
  }

  @Override
  public V put(K key, V value) {
    if (key == null) {
      logger.error("Your key is null");
    }
    root = put(root, key, value);
    root.color = BLACK;
    size++;
    return value;
  }

  private Node<K, V> put(Node<K, V> node, K key, V value) {
    if (node == null) {
      return new Node<K, V>(key, value, RED);
    }
    int compareResult = compare(key, node.key);
    if (compareResult < 0) {
      node.left = put(node.left, key, value);
    } else if (compareResult > 0) {
      node.right = put(node.right, key, value);
    } else {
      node.value = value;
    }
    if (isRed(node.right) && !isRed(node.left)) {
      node = turnLeft(node);
    }
    if (isRed(node.left) && isRed(node.left.left)) {
      node = turnRight(node);
    }
    if (isRed(node.left) && isRed(node.right)) {
      changeColors(node);
    }
    return node;
  }

  private int compare(K firstKey, K secondKey) {
    if (comparator != null) {
      return comparator.compare(firstKey, secondKey);
    }
    try {
      Comparable<? super K> cmpKey1 = (Comparable<? super K>) firstKey;
      return cmpKey1.compareTo(secondKey);
    } catch (ClassCastException e) {
      throw new ClassCastException(e.getMessage());
    }
  }

  private boolean isRed(Node<K, V> node) {
    if (node == null)
      return false;
    return node.color == RED;
  }

  private Node<K, V> turnLeft(Node<K, V> node) {
    assert (node != null) && isRed(node.right);
    Node<K, V> tempNode = node.right;
    node.right = tempNode.left;
    tempNode.left = node;
    tempNode.color = tempNode.left.color;
    tempNode.left.color = RED;
    return tempNode;
  }

  private Node<K, V> turnRight(Node<K, V> node) {
    assert (node != null) && isRed(node.left);
    Node<K, V> tempNode = node.left;
    node.left = tempNode.right;
    tempNode.right = node;
    tempNode.color = tempNode.right.color;
    tempNode.right.color = RED;
    return tempNode;
  }

  private void changeColors(Node<K, V> node) {
    node.color = !node.color;
    node.left.color = !node.left.color;
    node.right.color = !node.right.color;
  }

  @Override
  public V remove(Object key) {
    if (key == null) {
      logger.error("Your key is null");
    }
    if (containsKey(key)) {
      V value = get(key);
      if (!isRed(root.left) && !isRed(root.right)) {
        root.color = RED;
      }
      root = remove(root, (K) key);
      size--;
      if (!isEmpty()) {
        root.color = BLACK;
      }
      return value;
    }
    return null;
  }

  private Node<K, V> remove(Node<K, V> node, K key) {
    if (compare(key, node.key) < 0) {
      if (!isRed(node.left) && !isRed(node.left.left))
        node = moveRedLeft(node);
      node.left = remove(node.left, key);
    } else {
      if (isRed(node.left))
        node = turnRight(node);
      if (compare(key, node.key) == 0 && (node.right == null))
        return null;
      if (!isRed(node.right) && !isRed(node.right.left))
        node = moveRedRight(node);
      if (compare(key, node.key) == 0) {
        Node<K, V> x = min(node.right);
        node.key = x.key;
        node.value = x.value;
        node.right = removeMin(node.right);
      } else
        node.right = remove(node.right, key);
    }
    return balance(node);
  }

  private Node<K, V> moveRedLeft(Node<K, V> node) {
    changeColors(node);
    if (isRed(node.right.left)) {
      node.right = turnRight(node.right);
      node = turnLeft(node);
      changeColors(node);
    }
    return node;
  }

  private Node<K, V> moveRedRight(Node<K, V> node) {
    changeColors(node);
    if (isRed(node.left.left)) {
      node = turnRight(node);
      changeColors(node);
    }
    return node;
  }

  private Node<K, V> min(Node<K, V> node) {
    if (node.left == null) {
      return node;
    } else {
      return min(node.left);
    }
  }

  private Node<K, V> removeMin(Node<K, V> node) {
    if (node.left == null) {
      return null;
    }
    if (!isRed(node.left) && !isRed(node.left.left)) {
      node = moveRedLeft(node);
    }
    node.left = removeMin(node.left);
    return balance(node);
  }

  private Node<K, V> balance(Node<K, V> node) {
    if (isRed(node.right))
      node = turnLeft(node);
    if (isRed(node.left) && isRed(node.left.left))
      node = turnRight(node);
    if (isRed(node.left) && isRed(node.right))
      changeColors(node);
    return node;
  }

  @Override
  public void putAll(Map<? extends K, ? extends V> m) {

  }

  @Override
  public void clear() {

  }

  @Override
  public Set<K> keySet() {
    return null;
  }

  @Override
  public Collection<V> values() {
    return null;
  }

  @Override
  public Set<Entry<K, V>> entrySet() {
    return null;
  }

  public void print() {
    printNodes(root);
  }

  public void printNodes(Node<K, V> node) {
    if (node != null) {
      printNodes(node.left);
      logger.info(node.key + " - " + node.value);
      printNodes(node.right);
    }
  }
}
