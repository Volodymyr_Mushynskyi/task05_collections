package com.mushynskyi.mytree;

import java.util.Objects;

public class Plane implements Comparable<Plane> {
  private String name;

  public Plane(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return "Plane{" +
            "name='" + name + '\'' +
            '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Plane plane = (Plane) o;
    return Objects.equals(name, plane.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }

  @Override
  public int compareTo(Plane plane) {
    return this.getName().compareTo(plane.getName());
  }
}
