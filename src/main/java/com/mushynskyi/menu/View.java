package com.mushynskyi.menu;

import com.mushynskyi.genericstasks.Droid;
import com.mushynskyi.genericstasks.Ship;
import com.mushynskyi.logicaltasks.arrays.ArraysTasks;
import com.mushynskyi.logicaltasks.game.GameProcess;
import com.mushynskyi.myqueue.dequeue.MyDeq;
import com.mushynskyi.myqueue.dequeue.QDroid;
import com.mushynskyi.myqueue.priorityqueue.MyPriorityQueue;
import com.mushynskyi.myqueue.priorityqueue.Wallet;
import com.mushynskyi.mytree.MyTree;
import com.mushynskyi.mytree.Plane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Logger logger = LogManager.getLogger(View.class);
  private static Scanner input = new Scanner(System.in);

  public View() {
    menu = new LinkedHashMap<>();
    menu.put("1", "Press  1 - execute Generic tasks");
    menu.put("2", "Press  2 - execute Logical tasks (game and arrays)");
    menu.put("3", "Press  3 - execute Dequeue");
    menu.put("4", "Press  4 - execute PriorityQueue");
    menu.put("5", "Press  5 - execute My tree");
    menu.put("0", "Press  0 - exit");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::printGenericTasks);
    methodsMenu.put("2", this::printLogicalTasks);
    methodsMenu.put("3", this::printDequeue);
    methodsMenu.put("4", this::printPriorityQueue);
    methodsMenu.put("5", this::printMyTree);
  }

  private void printMenuAction() {
    System.out.println("--------------MENU-----------\n");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  public void printGenericTasks() {
    Ship<Droid> ship = new Ship<>();
    ship.put(new Droid("A"));
    ship.put(new Droid("C"));
    ship.put(new Droid("B"));
    ship.get(0);
  }

  public void printLogicalTasks() {
    new GameProcess().startGame();
    new ArraysTasks().createThirdArray();
  }

  public void printDequeue() {
    MyDeq<QDroid> myDeq = new MyDeq<>();
    myDeq.addFirst(new QDroid("A"));
    myDeq.addFirst(new QDroid("B"));
    myDeq.addFirst(new QDroid("C"));
    myDeq.addLast(new QDroid("H"));
    System.out.println();
    for (QDroid d : myDeq) {
      logger.info(d);
    }
    logger.info("Remove first value");
    myDeq.deleteFromTop();
    for (QDroid d : myDeq) {
      logger.info(d);
    }
  }

  public void printPriorityQueue() {
    MyPriorityQueue<Wallet> myPriorityQueue = new MyPriorityQueue<>();
    Wallet a = new Wallet("Mike", 50);
    myPriorityQueue.add(a);
    myPriorityQueue.add(new Wallet("Mo", 22));
    myPriorityQueue.add(new Wallet("Red", 100));
    myPriorityQueue.add(new Wallet("Sam", 15));
    myPriorityQueue.add(new Wallet("Sm", 5));
    myPriorityQueue.add(new Wallet("Sa", 150));
    myPriorityQueue.add(new Wallet("John", 60));
    myPriorityQueue.add(new Wallet("Jo", 200));
    myPriorityQueue.getAll();
    logger.info("Deleting one element");
    myPriorityQueue.remove(a);
    myPriorityQueue.getAll();
  }

  public void printMyTree() {
    MyTree<Plane, Integer> myTree = new MyTree<>();
    Plane plane = new Plane("A");
    myTree.put(new Plane("Q"), 2);
    myTree.put(new Plane("S"), 7);
    myTree.put(plane, 13);
    myTree.put(new Plane("C"), 27);
    myTree.put(new Plane("B"), 20);
    logger.info(myTree.get(plane));
    myTree.print();
    logger.info("Remove Mike");
    myTree.remove(plane);
    myTree.print();
  }

  public void showMenu() {
    String keyMenu;
    do {
      printMenuAction();
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("0"));
  }
}
