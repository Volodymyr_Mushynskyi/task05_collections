package com.mushynskyi.constants;

public class Constants {
  public static final Integer PQ_SIZE = 16;
  public static final Integer AR_SIZE = 10;
  public static final Integer DOOR_SIZE = 10;

  private Constants() {
  }
}
