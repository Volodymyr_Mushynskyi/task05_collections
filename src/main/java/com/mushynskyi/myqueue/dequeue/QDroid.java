package com.mushynskyi.myqueue.dequeue;

import java.util.Objects;

public class QDroid implements Comparable<QDroid> {

  private String name;

  public QDroid(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    QDroid QDroid = (QDroid) o;
    return Objects.equals(name, QDroid.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }

  @Override
  public String toString() {
    return "QDroid{" +
            "name='" + name + '\'' +
            '}';
  }

  @Override
  public int compareTo(QDroid QDroid) {
    return this.getName().compareTo(QDroid.getName());
  }
}
