package com.mushynskyi.logicaltasks.arrays;

import com.mushynskyi.constants.Constants;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.stream.IntStream;

public class ArraysTasks {

  private int[] firstArray = new int[Constants.AR_SIZE];
  private int[] secondArray = new int[Constants.AR_SIZE];
  private int[] tempArray = new int[Constants.AR_SIZE];
  private int[] thirdArray = new int[Constants.AR_SIZE];
  private Random random = new Random();

  private Logger logger = LogManager.getLogger(ArraysTasks.class);

  private void randomInitialization() {
    for (int i = 0; i < Constants.AR_SIZE; i++) {
      firstArray[i] = random.nextInt(10) + 1;
      secondArray[i] = random.nextInt(10) + 1;
    }
    logger.info("Initialized arrays");
    logger.info("First array :" + printArray(firstArray));
    logger.info("Second array :" + printArray(secondArray));
  }

  private void randomInitializationOfThirdArray() {
    thirdArray = new int[10];
    for (int i = 0; i < thirdArray.length; i++) {
      thirdArray[i] = random.nextInt(3) + 1;
    }
  }

  private String printArray(int[] array) {
    return java.util.Arrays.toString(array);
  }

  private void clearThirdArray() {
    for (int i = 0; i < thirdArray.length; i++) {
      thirdArray[i] = 0;
      tempArray[i] = 0;
    }
  }

  private void chooseDigitsFromTwoArrays() {
    int size = 0;

    for (int i = 0; i < Constants.AR_SIZE; i++) {
      for (int j = 0; j < Constants.AR_SIZE; j++) {
        if (firstArray[i] == secondArray[j]) {
          tempArray[size++] = secondArray[j];
          secondArray[j] = 0;
        }
      }
    }
  }

  private void chooseDigitsThatAreInOnlyInOneArrays() {
    for (int i = 0; i < thirdArray.length; i++) {
      for (int j = 0; j < firstArray.length; j++) {
        if (thirdArray[i] == firstArray[j]) {
          firstArray[j] = 0;
        }
      }
    }
  }

  private void taskA() {
    randomInitialization();
    chooseDigitsFromTwoArrays();
    thirdArray = IntStream.of(tempArray).distinct().filter(e -> e != 0).toArray();
    logger.info("Digits that are in two array :" + printArray(thirdArray));
    chooseDigitsThatAreInOnlyInOneArrays();
    firstArray = IntStream.of(firstArray).distinct().filter(e -> e != 0).toArray();
    secondArray = IntStream.of(secondArray).distinct().filter(e -> e != 0).toArray();
    clearThirdArray();
    thirdArray = ArrayUtils.addAll(firstArray, secondArray);
    logger.info("Digits that are only in one array :" + printArray(thirdArray));
  }

  private void deleteDigitsThatRepeatMoreThanTwoTimes() {
    int temp = 0;
    int count = 0;
    int trigger = 0;
    for (int i = 0; i < thirdArray.length; i++) {
      for (int j = 0; j < thirdArray.length; j++) {
        if (thirdArray[i] == thirdArray[j]) {
          ++count;
          if (count > 2) {
            thirdArray[j] = 0;
            trigger = 1;
            temp = thirdArray[i];
          }
        }
      }
      for (int j = 0; j < thirdArray.length; j++) {
        if (thirdArray[j] == temp) {
          thirdArray[j] = 0;
        }
      }
      count = 0;
      if (trigger == 1) {
        thirdArray[i] = 0;
        trigger = 0;
      }
    }
  }

  private void deleteDigitsThatGoInRow() {
    for (int i = 0; i < thirdArray.length - 1; i++) {
      if (thirdArray[i] == thirdArray[i + 1]) {
        thirdArray[i] = 0;
      }
    }
  }

  private void taskB() {
    logger.info("Delete all digits that repeat more than two times :" + printArray(thirdArray));
    deleteDigitsThatRepeatMoreThanTwoTimes();
    logger.info("Result :" + printArray(thirdArray));
  }

  private void taskC() {
    logger.info("Delete digits that go in row :" + printArray(thirdArray));
    deleteDigitsThatGoInRow();
    logger.info("Result :" + printArray(thirdArray));
  }

  public void createThirdArray() {
    taskA();
    clearThirdArray();
    randomInitializationOfThirdArray();
    taskB();
    clearThirdArray();
    randomInitializationOfThirdArray();
    taskC();
  }
}
